//
//  imgurPhoto.swift
//  FieldWireImageSearch
//
//  Created by Jigar Panchal on 7/14/17.
//  Copyright © 2017 Jigar Panchal. All rights reserved.
//

import Foundation



struct imgurPhoto{
    
    
    //MARK:Properties
    let _photoUrl:String
    
    
    //returns photo url to fetch image
    var photoUrl: NSURL {
        return NSURL(string: _photoUrl)!
    }
    
}
