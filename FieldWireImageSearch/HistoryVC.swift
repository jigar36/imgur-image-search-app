//
//  HistoryVC.swift
//  FieldWireImageSearch
//
//  Created by Jigar Panchal on 7/14/17.
//  Copyright © 2017 Jigar Panchal. All rights reserved.
//

import UIKit

class HistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableView: UITableView!
    var history: [History] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        
        getData()
        
        tableView.reloadData()
        
    }
    
    
    //MARK: TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return history.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        let note = history[indexPath.row]
        
        if note.isText == nil{
            return UITableViewCell()
        }else{
            cell.textLabel?.text = note.isText!
            return cell
        }
    }
    
    //MARK: Fetching data from CoreData Database
    
    func getData(){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        do{
            history = try context.fetch(History.fetchRequest())
        }catch{
            print("Fetching Failed...!")
        }
    }
    
}
