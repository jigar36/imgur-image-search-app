//
//  CollectionViewCell.swift
//  FieldWireImageSearch
//
//  Created by Jigar Panchal on 7/14/17.
//  Copyright © 2017 Jigar Panchal. All rights reserved.
//

import UIKit
import SDWebImage

class CollectionViewCell: UICollectionViewCell {
    
    //MARK: Properties
    @IBOutlet weak var collectionPhoto: UIImageView!
    
    
    // Setting corner radius to the collection view cell
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        
        layer.cornerRadius = 5.0
    }
    
    var imgurP:imgurPhoto!
    
    func configureCell(_ imgurP : imgurPhoto){
        self.imgurP = imgurP
        
        collectionPhoto.sd_setImage(with: imgurP.photoUrl as URL!)
    }
    
    
}
