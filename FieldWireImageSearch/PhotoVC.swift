//
//  PhotoVC.swift
//  FieldWireImageSearch
//
//  Created by Jigar Panchal on 7/14/17.
//  Copyright © 2017 Jigar Panchal. All rights reserved.
//

import UIKit

class PhotoVC: UIViewController {

    //MARK:Properties
    @IBOutlet weak var imageView: UIImageView!
    
    
    var imgurP: imgurPhoto?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if imgurP != nil {
             imageView.sd_setImage(with: imgurP!.photoUrl as URL!)
           
        }

    }
}
