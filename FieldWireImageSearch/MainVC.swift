//
//  ViewController.swift
//  FieldWireImageSearch
//
//  Created by Jigar Panchal on 7/14/17.
//  Copyright © 2017 Jigar Panchal. All rights reserved.
//

import UIKit
import Alamofire

class MainVC: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource, UISearchControllerDelegate, UISearchBarDelegate {

    //MARK: Properties
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var imgurP = [imgurPhoto]()
    var itemToEdit: History?
    
    
    // For reseting collection view and seaarch textbox
    func reset(){
        imgurP.removeAll(keepingCapacity: false);
        searchBar.text = ""
        searchBar.resignFirstResponder()
        collectionView.reloadData()
        self.title = "Imgur Search"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done

    }
    
    
    //MARK: CollectionView delegates
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell{
            
            cell.configureCell(imgurP[indexPath.row])
            return cell
        }
        else {
            return UICollectionViewCell()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "PhotoSegue", sender: imgurP[indexPath.row])
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgurP.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 105, height: 105)
        
    }
    
    // MARK - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PhotoSegue" {
            let photoViewController = segue.destination as! PhotoVC
            if let image = sender as? imgurPhoto {
                photoViewController.imgurP = image
            }
            
            
        }
        
    }
    
    //MARK: SearchBar Delegates
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    

    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == ""{
            

            imgurP.removeAll(keepingCapacity: false);
            downloadForecastData(searchBar.text!) {
                self.collectionView.reloadData()
            }
            view.endEditing(true)
            
        } else{

            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            
//                 var history: [History] = []
//                    do{
//                        history = try context.fetch(History.fetchRequest())
//                    }catch{
//                        print("Fetching Failed...!")
//                    }
//            
//                    if history.count > 6 {
//                        for object in history {
//                            context.delete(object)
//                            if history.count < 6{
//                                break
//                            }
//                        }
//                    }
            
            let notes: History!
            
            if itemToEdit == nil{
                
                notes = History(context: context)
                
            }else{
                notes = itemToEdit
            }
            
            notes.isText = searchText
            
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
   
            imgurP.removeAll(keepingCapacity: false);
            downloadForecastData(searchBar.text!) {
                self.collectionView.reloadData()
            }
            
        }
    }
    
    
    //MARK: Fetching Data from imgur
    func downloadForecastData(_ search:String, completed: @escaping DownloadComplete) {
        
        let url = BASE_URL + search
        Alamofire.request(url, method: .get, headers: headers).responseJSON { response in
            let result = response.result
                    if let dict = result.value as? Dictionary<String, AnyObject> {
                
                if let list = dict["data"] as? [Dictionary<String, AnyObject>] {
                    
                    for obj in list {
                        if let name = obj["link"] as? String {
                            
                            if name.contains(".jpg") || name.contains(".png") || name.contains(".gif"){
                                
                                let pht = imgurPhoto(_photoUrl: name)
                                
                                self.imgurP.append(pht)
                            }
                        }
                    }
                    
                }
            }
            completed()
        }
    }
    
    
    
    //MARK: Actions
    
    @IBAction func ResetBtnPressed(_ sender: UIBarButtonItem) {
        
        reset()
        
    }
}

